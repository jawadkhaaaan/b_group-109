library(tidyverse)
library(dplyr)
df <- read_csv("Trial_of_violent_crimes_by_courts.csv")
new_df = subset(df, Sub_Group_Name == "01. Murder" | Sub_Group_Name == "08. Robbery")
new_df$Trial_of_Violent_Crimes_by_Courts_Total<-as.numeric(new_df$Trial_of_Violent_Crimes_by_Courts_Total)
lapply(new_df,class)
dt <-new_df$Trial_of_Violent_Crimes_by_Courts_Total
dtMin=min(dt,na.rm=TRUE)
dtMax=max(dt,na.rm=TRUE)
dtMean=mean(dt,na.rm=TRUE)
dtSd=sd(dt,na.rm=TRUE)
df2<-spread(new_df,Sub_Group_Name,Trial_of_Violent_Crimes_by_Courts_Total)

Mydata = select(df2,`01. Murder`,`08. Robbery`)

pdf("visualization.pdf", width=10, height=8)
boxplot(Mydata[,1:2],col = c("red","green","blue","orange"),names=c("Murder","Robbery"),
        xlab="Crime",ylab="No of Trail Cases in India",
        main="Number of trail cases in court between Robbery and Murder in India from 2001 to 2010")


h <- hist(dt, breaks = 10, density = 50,
          col = "black", 
          xlab = "No of Trial Cases", 
          main = "Frequency of Trial Cases in India from 2001 to 2010") #you might want to tweak this
x <-seq(dtMin, dtMax,1)  #creates a sequence of numbers between first 2 params
y1 <-dnorm(x, mean=dtMean, sd=dtSd) #creates a theoretical normal distribution based on that
y1 <- y1 *diff(h$mids[1:2]) *length(dt) #a multiplier to make it fit is the histogram
lines(x, y1, col="red")
dev.off()

